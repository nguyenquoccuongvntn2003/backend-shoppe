<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Foundation\Auth\User;
use Pros\CodeBase\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function all() 
    {
        return User::all();
    }
}
